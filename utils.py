import importlib.util
import matplotlib
import sys
import os
import torch as tr
import numpy as np
from typing import Dict
from nwutils.kps import frameKeypointer
from media_processing_lib.image import image_resize
from vke.face import imgGetFaceBbox
from nwutils.bbox import imgGetBboxPadding, squareBbox

model = None

def getModel(config_path: str, checkpoint_path: str):
	global model
	if model is not None:
		print("Model already instantaited. Returning early.")
		return model

	mpl_backend = matplotlib.get_backend()
	first_order_path = f"{os.environ['FOM_PATH']}"
	sys.path.append(first_order_path)
	demo_path = f"{first_order_path}/demo.py"
	spec = importlib.util.spec_from_file_location("module.name", demo_path)
	fom = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(fom)
	# FOM sets backend to Agg, so we cannot use webcam/show plots in real time.
	matplotlib.use(mpl_backend)

	cpu = not tr.cuda.is_available()
	print(f"Importing model using:\n- First Order Model Path: '{first_order_path}'\n"
		  f"- Checkpoint: '{checkpoint_path}'\n- Config: '{config_path}'\n- GPU: {not cpu}")
	generator, kp_detector = fom.load_checkpoints(config_path, checkpoint_path, cpu=cpu)

	class Model: pass
	model = Model()
	model.generator = generator
	model.kp_detector = kp_detector
	return model

def fTry(f, *args, **kwargs):
	N = 100
	while True:
		i = 0
		try:
			res = f(*args, **kwargs)
			return res
		except Exception as e:
			print(f"[fTry] f:{f} failed with exception after N={N} tries: {e}")
			i += 1
			if i < N:
				continue
			raise Exception(e)

def presentDrivingFrame(imageItems: Dict, height: int=500, width: int=500):
	# [-1 : 1] => [0 : 256]
	modelKps = np.int32((imageItems["modelKps"]["value"][0] + 1) / 2 * 256)
	frame = frameKeypointer(imageItems["bboxImage"], modelKps, color=(255, 0, 0))
	if "bboxFaceKps" in imageItems:
		frame = frameKeypointer(frame, imageItems["bboxFaceKps"])
	frame = image_resize(frame, height=height, width=width, interpolation="lanczos")
	return frame

# @brief Automatically get the padding of an image w.r.t the detected bounding box. Basically, if we have an image of
#  H=480, W=640 and we detect a face at bbox x1=
def getPadding(image:np.ndarray):
	assert image.dtype == np.uint8
	bbox = imgGetFaceBbox(image, "face_alignment")
	bbox = squareBbox(bbox)
	padding = imgGetBboxPadding(image, bbox)
	# Adapt the padding to 256x256
	H, W = image.shape[0 : 2]
	padding = padding * 256 / [W, W, H, H]
	padding = padding.astype(np.int32)
	print(f"[getPadding] Left: {padding[0]}. Right: {padding[1]}. Top: {padding[2]}. Bottom: {padding[3]}")
	return padding
